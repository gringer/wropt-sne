from flask import Flask, flash, render_template, request, redirect, url_for, send_from_directory
import os
import subprocess

UPLOAD_FOLDER = '/usr/src/app/app/uploads'
ALLOWED_EXTENSIONS = '.csv'

app = Flask(__name__)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SECRET_KEY'] = 'jaeg1Xeith1AeS1eefah'

@app.route("/", methods=['GET', 'POST'])

def upload_file():
    if request.method == 'POST':
       if 'file' not in request.files:
          flash('No file part')
          return redirect(request.url)
       file = request.files['file']
       if file.filename == '':
          flash('No selected file')
          return redirect(request.url)
       if file:
          filename = 'uploaded_file.csv'
          fullFileName = os.path.join(app.config['UPLOAD_FOLDER'], filename)
          fullOutFileName = os.path.join(app.config['UPLOAD_FOLDER'], 'tsne_results.csv')
          file.save(fullFileName)
          commandLine = list(('python',
                              'Multicore-opt-SNE/OptSNE/run/run_optsne.py',
                              '--data', fullFileName,
                              '--outfile', fullOutFileName,
                              '--n_threads', '10',
                              '--learning_rate', request.form['learning_rate'],
                              '--n_iter_early_exag', request.form['n_iter_early_exag'],
                              '--n_iter', request.form['n_iter'],
                              '--perp', request.form['perp'],
                              '--theta', request.form['theta'],
                              '--optsne_end', request.form['optsne_end'],
                              '--early_exaggeration', request.form['early_exaggeration'],
                              '--n_obs', request.form['n_obs'],
                              '--seed', request.form['seed'],
                              '--verbose', request.form['verbose']))
          runProcess = subprocess.call(commandLine)
          return send_from_directory(app.config['UPLOAD_FOLDER'], filename='tsne_results.csv',
                                     as_attachment=True, mimetype='text/csv')
    return render_template('upload.html')

def hello(name=None):
    return render_template('index.html', name=name)

def index():
  return render_template('index.html', name=None)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
