FROM python:2

WORKDIR /usr/src/app

RUN /usr/local/bin/python -m pip install --upgrade pip

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
RUN git clone https://github.com/rafaelmessias/Multicore-opt-SNE.git

RUN pip install --no-cache-dir -e Multicore-opt-SNE

RUN echo $(pwd)
RUN ls -l

COPY . /usr/src/app

ENTRYPOINT [ "python", "app/app.py" ]
